﻿using get_a_job_backend.Models;
using get_a_job_backend.Models.DataManagers;
using get_a_job_backend.Models.Repository;
using get_a_job_backend.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace get_a_job_backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly PostDataManager _dataRepository;
        public PostsController(PostDataManager dataRepository)
        {
            _dataRepository = dataRepository;
        }

        //GET: api/Posts
        [HttpGet]
        public IActionResult Get()
        {
            IEnumerable<Post> posts = _dataRepository.GetAll();
            return Ok(posts);
        }

        //GET: api/Posts/published/5
        [HttpGet("published/{id}")]
        public IActionResult GetAllPublished(Guid id)
        {
            IEnumerable<PostViewModel> posts = _dataRepository.GetPostsBySectionIdPublished(id);
            return Ok(posts);
        }

        //GET: api/Posts/pending/5
        [HttpGet("pending/{id}")]
        public IActionResult GetAllPending(Guid id)
        {
            IEnumerable<PostViewModel> posts = _dataRepository.GetPostsBySectionIdPending(id);
            return Ok(posts);
        }

        //POST: api/Posts
        [HttpPost]
        public IActionResult Post([FromBody] Post post)
        {
            if (post == null)
            {
                return BadRequest("Post is null.");
            }

            var newPost = new Post
            {
                UserId = post.UserId,
                SectionId = post.SectionId,
                PostText = post.PostText,
                PostedDate = post.PostedDate,
                Status = post.Status,
            };

            return Created("Success", _dataRepository.Create(newPost));
        }

        //PUT: api/Posts/5
        [HttpPut("{id}")]
        public IActionResult Put(Guid id, [FromBody] Post post)
        {
            if (post == null)
            {
                return BadRequest("Section is null.");
            }

            Post postToUpdate = _dataRepository.Get(id);

            if (postToUpdate == null)
            {
                return NotFound("Could not find post to update.");
            }

            _dataRepository.Update(postToUpdate, post);
            return NoContent();
        }

        //DELETE: api/Posts/5
        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            Post post = _dataRepository.Get(id);

            if (post == null)
            {
                return NotFound("Post not found.");
            }

            _dataRepository.Delete(post);
            return Ok("Success");
        }
    }
}
