﻿using get_a_job_backend.Models;
using get_a_job_backend.Models.DataManagers;
using get_a_job_backend.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace get_a_job_backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EducationsController : ControllerBase
    {
        private readonly EducationDataManager _dataRepository;

        public EducationsController(EducationDataManager dataRepository)
        {
            _dataRepository = dataRepository;
        }

        //GET: api/Educations
        [HttpGet]
        public IActionResult Get()
        {
            IEnumerable<Education> educations = _dataRepository.GetAll();
            return Ok(educations);
        }

        //GET: api/Educations/5
        [HttpGet("{id}")]
        public ActionResult<IEnumerable<EducationViewModel>> GetEducation(Guid id)
        {
            var education = _dataRepository.GetEducationByResumeId(id);

            if (education == null)
            {
                return NotFound("The education was not found.");
            }

            return Ok(education);
        }

        //POST: api/Educations
        [HttpPost]
        public IActionResult Post([FromBody] EducationViewModel education)
        {
            if (education == null)
            {
                return BadRequest("Education is null.");
            }

            var newEducation = new Education
            {
                ResumeId = education.ResumeId,
                Institution = education.Institution,
                StudyField = education.StudyField,
                EducationCity = education.EducationCity,
                EducationCountry = education.EducationCountry,
                EducationDescription = education.EducationDescription,
                StartDate = education.StartDate,
                EndDate = education.EndDate,
            };

            return Created("success", _dataRepository.Create(newEducation));
        }

        //PUT: api/Educations/5
        [HttpPut("{id}")]
        public IActionResult Put(Guid id, [FromBody] Education education)
        {
            if (education == null)
            {
                return BadRequest("Education is null.");
            }

            Education educationToUpdate = _dataRepository.Get(id);

            if (educationToUpdate == null)
            {
                return NotFound("Could not find education to update.");
            }

            _dataRepository.Update(educationToUpdate, education);
            return NoContent();
        }

        //DELETE: api/Educations/5
        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            Education education = _dataRepository.Get(id);

            if (education == null)
            {
                return NotFound("Education not found.");
            }

            _dataRepository.Delete(education);
            return Ok("Success");
        }
    }
}
