﻿using get_a_job_backend.Helpers;
using get_a_job_backend.Models;
using get_a_job_backend.Models.DataManagers;
using get_a_job_backend.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;

namespace get_a_job_backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UserDataManager _dataRepository;
        public readonly JwtService _jwtService;

        public UsersController(UserDataManager dataRepository, JwtService jwtService)
        {
            _dataRepository = dataRepository;
            _jwtService = jwtService;
        }

        // GET: api/Users
        [HttpGet]
        public IActionResult Get()
        {
            IEnumerable<User> users = _dataRepository.GetAll();
            return Ok(users);
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public ActionResult<IEnumerable<UserViewModel>> GetUser(Guid id)
        {
            var user = _dataRepository.Get(id);

            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        // PUT: api/Users/5
        [HttpPut("{id}")]
        public IActionResult Put(Guid id, [FromBody] User user)
        {
            if (user == null)
            {
                return BadRequest("User is null");
            }

            User userToUpdate = _dataRepository.Get(id);

            if (userToUpdate == null)
            {
                return NotFound("The user record could not be found");
            }

            _dataRepository.Update(userToUpdate, user);
            return NoContent();
        }

        // POST: api/Users
        [HttpPost]
        public IActionResult Post([FromBody] User user)
        {
            if (user == null)
            {
                return BadRequest("User is null");
            }

            _dataRepository.Create(user);

            return CreatedAtRoute("GetUser", new { id = user.UserId }, user);
        }

        [HttpPost("Register")]
        public IActionResult Register(SignUpViewModel entity)
        {
            var isEmailValid = _dataRepository.IsEmailValid(entity.UserEmail);

            if (!isEmailValid)
            {
                return BadRequest(new { message = "Email address is invalid." });
            }

            var existingAccount = _dataRepository.ExistingAccount(entity.UserEmail);

            if (existingAccount != null)
            {
                return BadRequest(new { message = "Email address is already used." });
            }

            var user = new User
            {
                UserEmail = entity.UserEmail,
                UserPassword = BCrypt.Net.BCrypt.HashPassword(entity.UserPassword),
                UserFirstName = entity.UserFirstName,
                UserLastName = entity.UserLastName,
                UserGoal1 = string.Empty,
                UserGoal2 = string.Empty,
                UserGoal3 = string.Empty
            };

            _dataRepository.Create(user);

            return Ok(new { message = "Success" });
        }

        [HttpPost("Login")]
        public IActionResult Login(SignInViewModel entity)
        {
            var user = _dataRepository.GetByEmail(entity.UserEmail);

            if (user == null)
            {
                return BadRequest(new { message = "User does not exist" });
            }

            if (!BCrypt.Net.BCrypt.Verify(entity.UserPassword, user.UserPassword))
            {
                return BadRequest(new { message = "Wrong password" });
            }

            var jwt = _jwtService.Generate(user.UserId);
            Response.Cookies.Append("jwt", jwt, new CookieOptions { HttpOnly = true });

            return Ok(new { message = "Success" });
        }

        [HttpGet("user")]
        public IActionResult GetUser()
        {
            try
            {
                var jwt = Request.Cookies["jwt"];

                var token = _jwtService.Verify(jwt);

                Guid userid = Guid.Parse(token.Issuer);

                var user = _dataRepository.Get(userid);

                return Ok(user);
            }
            catch (Exception)
            {
                return Unauthorized();
            }
        }

        [HttpPost("Logout")]
        public IActionResult Logout()
        {
            Response.Cookies.Delete("jwt");
            return Ok(new { message = "success" });
        }


        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            User user = (User)_dataRepository.Get(id);

            if (user == null)
            {
                return NotFound("The user record could not be found");
            }

            _dataRepository.Delete(user);
            return Ok("success");
        }
    }
}
