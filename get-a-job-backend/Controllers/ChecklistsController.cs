﻿using get_a_job_backend.Models;
using get_a_job_backend.Models.DataManagers;
using get_a_job_backend.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace get_a_job_backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChecklistsController : ControllerBase
    {
        private readonly ChecklistDataManager _dataRepository;

        public ChecklistsController(ChecklistDataManager dataRepository)
        {
            _dataRepository = dataRepository;
        }

        //GET: api/Checklists
        [HttpGet]
        public IActionResult Get()
        {
            IEnumerable<Checklist> checklists = _dataRepository.GetAll();
            return Ok(checklists);
        }

        //GET: api/Checklists/5
        [HttpGet("{id}")]
        public ActionResult<IEnumerable<ChecklistViewModel>> GetChecklist(Guid id)
        {
            var checklist = _dataRepository.Get(id);

            if (checklist == null)
            {
                return NotFound("The checklist record was not found.");
            }

            return Ok(checklist);
        }

        //GET: api/Checklists/User/5
        [HttpGet("User/{id}")]
        public ActionResult<IEnumerable<ChecklistViewModel>> GetChecklistByUserId(Guid id)
        {
            var checklist = _dataRepository.GetChecklistsByUserId(id);

            if (checklist == null)
            {
                return NotFound("The checklist record was not found.");
            }

            return Ok(checklist);
        }

        //GET: api/Checklists/Dashboard/5
        [HttpGet("Dashboard/{id}")]
        public ActionResult<IEnumerable<DashboardChecklistViewModel>> GetChecklistsForDashboard(Guid id)
        {
            var checklist = _dataRepository.GetChecklistsForDashboard(id);

            if (checklist == null)
            {
                return NotFound("The dashboard checklist records were not found.");
            }

            return Ok(checklist);
        }

        //PUT: api/Checklists/5
        [HttpPut("{id}")]
        public IActionResult Put(Guid id, [FromBody] Checklist checklist)
        {
            if(checklist == null)
            {
                return BadRequest("Checklist is null.");
            }

            Checklist checklistToUpdate = _dataRepository.Get(id);

            if(checklistToUpdate == null)
            {
                return NotFound("The checklist record could not be found.");
            }

            _dataRepository.Update(checklistToUpdate, checklist);
            return NoContent();
        }

        //POST: api/Checklists
        [HttpPost]
        public IActionResult Post([FromBody] ChecklistViewModel checklist)
        {
            if(checklist == null)
            {
                return BadRequest("Checklist is null.");
            }

            var newChecklist = new Checklist
            {
                ChecklistName = checklist.ChecklistName,
                UserId = checklist.UserId
            };

            return Created("success", _dataRepository.Create(newChecklist));
        }

        //DELETE: api/Checklists/5
        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            Checklist checklist = (Checklist)_dataRepository.Get(id);

            if (checklist == null)
            {
                return NotFound("The checklist record could not be found.");
            }

            _dataRepository.Delete(checklist);
            return Ok("Success");
        }
    }
}
