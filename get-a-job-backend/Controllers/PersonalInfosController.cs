﻿using get_a_job_backend.Models.Repository;
using get_a_job_backend.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using get_a_job_backend.Models.DataManagers;
using get_a_job_backend.Models.ViewModels;
using System;

namespace get_a_job_backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonalInfosController : ControllerBase
    {


        private readonly PersonalInfoDataManager _dataRepository;

        public PersonalInfosController(PersonalInfoDataManager dataRepository)
        {
            _dataRepository = dataRepository;
        }

        //GET: api/PersonalInfos
        [HttpGet]
        public IActionResult Get()
        {
            IEnumerable<PersonalInfo> personalInfo = _dataRepository.GetAll();
            return Ok(personalInfo);
        }

        //GET: api/PersonalInfos/5
        [HttpGet("{id}")]
        public ActionResult<PersonalInfoViewmodel> GetPersonalInfo(Guid id)
        {
            var personalInfo = _dataRepository.GetPersonalInfoByResumeId(id);

            if (personalInfo == null)
            {
                return NotFound("The personal info was not found.");
            }

            return Ok(personalInfo);
        }

        //POST: api/PersonalInfos
        [HttpPost]
        public IActionResult Post([FromBody] PersonalInfoViewmodel personalInfo)
        {
            if (personalInfo == null)
            {
                return BadRequest("Personal info is null.");
            }

            var newInfo = new PersonalInfo
            {
                ResumeId = personalInfo.ResumeId,
                FirstName = personalInfo.FirstName,
                LastName = personalInfo.LastName,
                Email = personalInfo.Email,
                LinkedInLink = personalInfo.LinkedInLink,
                Address = personalInfo.Address,
                PhoneNumber = personalInfo.PhoneNumber,
                Description = personalInfo.Description,
            };

            return Created("Success", _dataRepository.Create(newInfo));
        }

        //PUT: api/PersonalInfos/5
        [HttpPut("{id}")]
        public IActionResult Put(Guid id, [FromBody] PersonalInfo personalInfo)
        {
            if (personalInfo == null)
            {
                return BadRequest("Personal info is null.");
            }

            PersonalInfo infoToUpdate = _dataRepository.Get(id);

            if (infoToUpdate == null)
            {
                return NotFound("Could not find personal info to update.");
            }

            _dataRepository.Update(infoToUpdate, personalInfo);
            return NoContent();
        }

        //DELETE: api/PersonalInfos/5
        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            PersonalInfo personalinfo = _dataRepository.Get(id);

            if (personalinfo == null)
            {
                return NotFound("Personal info not found.");
            }

            _dataRepository.Delete(personalinfo);
            return Ok("Success");
        }


    }
}
