﻿using get_a_job_backend.Models;
using get_a_job_backend.Models.DataManagers;
using get_a_job_backend.Models.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace get_a_job_backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ResumesController : ControllerBase
    {
        private readonly ResumeDataManager _dataRepository;

        public ResumesController(ResumeDataManager dataRepository)
        {
            _dataRepository = dataRepository;
        }

        //GET: api/Resumes
        [HttpGet]
        public IActionResult Get()
        {
            IEnumerable<Resume> resumes = _dataRepository.GetAll();
            return Ok(resumes);
        }

        //GET: api/Resumes/5
        [HttpGet("{id}")]
        public ActionResult<ResumeViewModel> GetResume(Guid id)
        {
            var resume = _dataRepository.Get(id);

            if (resume == null)
            {
                return NotFound("The resume was not found.");
            }

            return Ok(resume);
        }

        //GET: api/Resumes/User/5
        [HttpGet("User/{id}")]
        public ActionResult<IEnumerable<ResumeViewModel>> GetResumesByUserId(Guid id)
        {
            var resumes = _dataRepository.GetResumesByUserId(id);

            if (resumes == null)
            {
                return NotFound("The resumes record was not found.");
            }

            return Ok(resumes);
        }


        //POST: api/Resumes
        [HttpPost]
        public IActionResult Post([FromBody] ResumeViewModel resume)
        {
            if(resume == null)
            {
                return BadRequest("Resume is null.");
            }

            var newResume = new Resume
            {
                UserId = resume.UserId,
                ResumeName = resume.ResumeName,
                CreationDate = resume.CreationDate
            };

            return Created("success", _dataRepository.Create(newResume));
        }

        //PUT: api/Resumes/5
        [HttpPut("{id}")]
        public IActionResult Put(Guid id, [FromBody] Resume resume)
        {
            if(resume == null)
            {
                return BadRequest("Resume is null.");
            }

            Resume resumeToUpdate = _dataRepository.Get(id);

            if(resumeToUpdate == null)
            {
                return NotFound("Could not find resume to update.");
            }

            _dataRepository.Update(resumeToUpdate, resume);
            return NoContent();
        }

        //DELETE: api/Resumes/5
        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            Resume resume = _dataRepository.Get(id);

            if(resume == null)
            {
                return NotFound("Resume not found.");
            }

            _dataRepository.Delete(resume);
            return Ok("Success");
        }
    }
}
