﻿using get_a_job_backend.Models;
using get_a_job_backend.Models.DataManagers;
using get_a_job_backend.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using static System.Collections.Specialized.BitVector32;
using Section = get_a_job_backend.Models.Section;

namespace get_a_job_backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SectionsController : ControllerBase
    {
        private readonly SectionDataManager _dataRepository;
        public SectionsController(SectionDataManager dataRepository)
        {
            _dataRepository = dataRepository;
        }

        //GET: api/Sections
        [HttpGet]
        public IActionResult Get()
        {
            IEnumerable<Section> sections = _dataRepository.GetAll();
            return Ok(sections);
        }

        //GET: api/Sections/published
        [HttpGet("published")]
        public IActionResult GetAllPublished()
        {
            IEnumerable<Section> sections = _dataRepository.GetAllPublished();
            return Ok(sections);
        }

        //GET: api/Sections/pending
        [HttpGet("pending")]
        public IActionResult GetAllPending()
        {
            IEnumerable<Section> sections = _dataRepository.GetAllPending();
            return Ok(sections);
        }

        //POST: api/Sections
        [HttpPost]
        public IActionResult Post([FromBody] Section section)
        {
            if (section == null)
            {
                return BadRequest("Section is null.");
            }

            var newSection = new Section
            {
                UserId = section.UserId,
                SectionName = section.SectionName,
                Status = section.Status,
            };

            return Created("Success", _dataRepository.Create(newSection));
        }

        //PUT: api/Sections/5
        [HttpPut("{id}")]
        public IActionResult Put(Guid id, [FromBody] Section section)
        {
            if (section == null)
            {
                return BadRequest("Section is null.");
            }

            Section sectionToUpdate = _dataRepository.Get(id);

            if (sectionToUpdate == null)
            {
                return NotFound("Could not find section to update.");
            }

            _dataRepository.Update(sectionToUpdate, section);
            return NoContent();
        }

        //DELETE: api/Sections/5
        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            Section section = _dataRepository.Get(id);

            if (section == null)
            {
                return NotFound("Section not found.");
            }

            _dataRepository.Delete(section);
            return Ok("Success");
        }
    }
}
