﻿using get_a_job_backend.Models;
using get_a_job_backend.Models.DataManagers;
using get_a_job_backend.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace get_a_job_backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventsController : ControllerBase
    {
        private readonly EventDataManager _dataRepository;

        public EventsController(EventDataManager dataRepository)
        {
            _dataRepository = dataRepository;
        }

        //GET: api/Events
        [HttpGet]
        public IActionResult Get()
        {
            IEnumerable<Event> events = _dataRepository.GetAll();
            return Ok(events);
        }

        //GET: api/Events/5
        [HttpGet("{id}")]
        public ActionResult<IEnumerable<EventViewModel>> GetEvents(Guid id)
        {
            var events = _dataRepository.GetEventsByUserId(id);

            if (events == null)
            {
                return NotFound("The events were not found.");
            }

            return Ok(events);
        }

        //POST: api/Events
        [HttpPost]
        public IActionResult Post([FromBody] EventViewModel ev)
        {
            if (ev == null)
            {
                return BadRequest("Event is null.");
            }

            var newEvent = new Event
            {
                UserId = ev.UserId,
                EventName = ev.EventName,
                EventStartDate = ev.EventStartDate,
                EventStopDate = ev.EventStopDate,
                EventLocation = ev.EventLocation,
                EventColor = ev.EventColor,
                EventDetails = ev.EventDetails,
            };

            return Created("Success", _dataRepository.Create(newEvent));
        }

        //PUT: api/Events/5
        [HttpPut("{id}")]
        public IActionResult Put(Guid id, [FromBody] Event ev)
        {
            if (ev == null)
            {
                return BadRequest("Event is null.");
            }

            Event eventToUpdate = _dataRepository.Get(id);

            if (eventToUpdate == null)
            {
                return NotFound("Could not find event to update.");
            }

            _dataRepository.Update(eventToUpdate, ev);
            return NoContent();
        }

        //DELETE: api/Events/5
        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            Event ev = _dataRepository.Get(id);

            if (ev == null)
            {
                return NotFound("Event not found.");
            }

            _dataRepository.Delete(ev);
            return Ok("Success");
        }
    }
}
