﻿using get_a_job_backend.Models;
using get_a_job_backend.Models.DataManagers;
using get_a_job_backend.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace get_a_job_backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LanguagesController : ControllerBase
    {
        private readonly LanguageDataManager _dataRepository;

        public LanguagesController(LanguageDataManager dataRepository)
        {
            _dataRepository = dataRepository;
        }

        //GET: api/Languages
        [HttpGet]
        public IActionResult Get()
        {
            IEnumerable<Language> language = _dataRepository.GetAll();
            return Ok(language);
        }

        //GET: api/Languages/5
        [HttpGet("{id}")]
        public ActionResult<IEnumerable<LanguageViewModel>> GetLanguages(Guid id)
        {
            var languages = _dataRepository.GetLanguagesByResumeId(id);

            if (languages == null || !languages.Any())
            {
                return NotFound("The languages were not found.");
            }

            return Ok(languages);
        }

        //POST: api/Languages
        [HttpPost]
        public IActionResult Post([FromBody] IEnumerable<LanguageViewModel> languages)
        {
            if (languages == null)
            {
                return BadRequest("Skills is null.");
            }

            foreach (var language in languages)
            {
                var newLanguage = new Language
                {
                    ResumeId = language.ResumeId,
                    LanguageName = language.LanguageName,
                    LanguageLevel = language.LanguageLevel,
                };

                _dataRepository.Create(newLanguage);
            }

            return Ok("Success.");
        }

        //PUT: api/Languages
        [HttpPut]
        public IActionResult Put([FromBody] IEnumerable<Language> languages)
        {
            if (languages == null)
            {
                return BadRequest("Language is null.");
            }

            foreach (var language in languages)
            {
                Language languageToUpdate = _dataRepository.Get(language.LanguageId);

                if (languageToUpdate == null)
                {
                    return NotFound("Could not find language to update.");
                }

                _dataRepository.Update(languageToUpdate, language);
            }
            return NoContent();
        }

        //DELETE: api/Languages/5
        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            Language language = _dataRepository.Get(id);

            if (language == null)
            {
                return NotFound("Language not found.");
            }

            _dataRepository.Delete(language);
            return Ok("Success");
        }
    }
}
