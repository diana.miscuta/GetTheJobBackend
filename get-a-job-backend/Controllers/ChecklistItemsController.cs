﻿using get_a_job_backend.Models;
using get_a_job_backend.Models.DataManagers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace get_a_job_backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChecklistItemsController : ControllerBase
    {
        private readonly ChecklistItemDataManager _dataRepository;

        public ChecklistItemsController (ChecklistItemDataManager dataRepository)
        {
            _dataRepository = dataRepository;
        }

        //GET: api/ChecklistItems
        [HttpGet]
        public IActionResult Get()
        {
            IEnumerable<ChecklistItem> items = _dataRepository.GetAll();
            return Ok(items);
        }

        //GET: api/ChecklistItems/5
        [HttpGet("{id}")]
        public ActionResult<IEnumerable<ChecklistItem>> GetItem(Guid id)
        {
            var item = _dataRepository.Get(id);

            if(item == null)
            {
                return NotFound("The checklist item record could not be found.");
            }

            return Ok(item);
        }

        //GET: api/ChecklistItems/Checklist/5
        [HttpGet("checklist/{id}")]
        public ActionResult<IEnumerable<ChecklistItem>> GetItemsByChecklistId(Guid id)
        {
            IEnumerable<ChecklistItem> items = _dataRepository.GetByChecklistId(id);

            if(items == null)
            {
                return NotFound("The checklist id could not be found.");
            }

            return Ok(items);
        }

        //PUT: api/ChecklistItems/5
        [HttpPut("{id}")]
        public IActionResult Put(Guid id, ChecklistItem item)
        {
            if(item == null)
            {
                return BadRequest("Item is null.");
            }

            var itemToUpdate = _dataRepository.Get(id);

            if(itemToUpdate == null)
            {
                return NotFound("The checklist item record could not be found.");
            }

            _dataRepository.Update(itemToUpdate, item);
            return NoContent();
        }

        //POST: api/ChecklistItems
        [HttpPost]
        public IActionResult Post([FromBody] ChecklistItem item)
        {
            if(item == null)
            {
                return BadRequest("Item is null.");
            }

            var newItem = new ChecklistItem
            {
                ChecklistId = item.ChecklistId,
                Text = item.Text,
                Status = item.Status
            };

            return Created("Success", _dataRepository.Create(newItem));
        }

        //DELETE: api/ChecklistItems/5
        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            ChecklistItem itemToDelete = (ChecklistItem)_dataRepository.Get(id);

            if(itemToDelete == null)
            {
                return NotFound("The checklist item record could not be found.");
            }

            _dataRepository.Delete(itemToDelete);
            return Ok("Success");
        }
    }
}
