﻿using get_a_job_backend.Models;
using get_a_job_backend.Models.DataManagers;
using get_a_job_backend.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace get_a_job_backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WorkExperiencesController : ControllerBase
    {
        private readonly WorkExperienceDataManager _dataRepository;

        public WorkExperiencesController(WorkExperienceDataManager dataRepository)
        {
            _dataRepository = dataRepository;
        }

        //GET: api/WorkExperiences
        [HttpGet]
        public IActionResult Get()
        {
            IEnumerable<WorkExperience> workExperiences = _dataRepository.GetAll();
            return Ok(workExperiences);
        }

        //GET: api/WorkExperiences/5
        [HttpGet("{id}")]
        public ActionResult<WorkExperienceViewModel> GetWorkExperience(Guid id)
        {
            var workExperience = _dataRepository.GetWorkExperienceByResumeId(id);

            if (workExperience == null)
            {
                return NotFound("The work experience was not found.");
            }

            return Ok(workExperience);
        }

        //POST: api/WorkExperiences
        [HttpPost]
        public IActionResult Post([FromBody] WorkExperienceViewModel workExperience)
        {
            if (workExperience == null)
            {
                return BadRequest("Work experience is null.");
            }

            var newWorkExperience = new WorkExperience
            {
                ResumeId = workExperience.ResumeId,
                JobTitle = workExperience.JobTitle,
                JobCity = workExperience.JobCity,
                JobCountry = workExperience.JobCountry,
                JobDescription = workExperience.JobDescription,
                StartDate = workExperience.StartDate,
                EndDate = workExperience.EndDate,
            };

            return Created("Success", _dataRepository.Create(newWorkExperience));
        }

        //PUT: api/WorkExperiences/5
        [HttpPut("{id}")]
        public IActionResult Put(Guid id, [FromBody] WorkExperience workExperience)
        {
            if (workExperience == null)
            {
                return BadRequest("Work experience is null.");
            }

            WorkExperience workToUpdate = _dataRepository.Get(id);

            if (workToUpdate == null)
            {
                return NotFound("Could not find work experience to update.");
            }

            _dataRepository.Update(workToUpdate, workExperience);
            return NoContent();
        }

        //DELETE: api/WorkExperiences/5
        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            WorkExperience workExperience = _dataRepository.Get(id);

            if (workExperience == null)
            {
                return NotFound("Work experience not found.");
            }

            _dataRepository.Delete(workExperience);
            return Ok("Success");
        }
    }
}
