﻿using get_a_job_backend.Models;
using get_a_job_backend.Models.DataManagers;
using get_a_job_backend.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace get_a_job_backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SkillsController : ControllerBase
    {
        private readonly SkillDataManager _dataRepository;

        public SkillsController(SkillDataManager dataRepository)
        {
            _dataRepository = dataRepository;
        }

        //GET: api/Skills
        [HttpGet]
        public IActionResult Get()
        {
            IEnumerable<Skill> skill = _dataRepository.GetAll();
            return Ok(skill);
        }

        //GET: api/Skills/5
        [HttpGet("{id}")]
        public ActionResult<IEnumerable<SkillViewModel>> GetSkill(Guid id)
        {
            var skills = _dataRepository.GetSkillsByResumeId(id);

            if (skills == null || !skills.Any())
            {
                return NotFound("The skills were not found.");
            }

            return Ok(skills);
        }

        //POST: api/Skills
        [HttpPost]
        public IActionResult Post([FromBody] IEnumerable<SkillViewModel> skills)
        {
            if (skills == null)
            {
                return BadRequest("Skills is null.");
            }

            foreach (var skill in skills)
            {
                var newSkill = new Skill
                {
                    ResumeId = skill.ResumeId,
                    SkillName = skill.SkillName,
                };

                _dataRepository.Create(newSkill);
            }

            return Ok("Success.");
        }

        //PUT: api/Skills
        [HttpPut]
        public IActionResult Put([FromBody] IEnumerable<Skill> skills)
        {
            if (skills == null)
            {
                return BadRequest("Skill is null.");
            }

            foreach(var skill in skills)
            {
                Skill skillToUpdate = _dataRepository.Get(skill.SkillId);

                if (skillToUpdate == null)
                {
                    return NotFound("Could not find skill to update.");
                }

                _dataRepository.Update(skillToUpdate, skill);
            }

            return NoContent();
        }

        //DELETE: api/Skills/5
        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            Skill skill = _dataRepository.Get(id);

            if (skill == null)
            {
                return NotFound("Skill not found.");
            }

            _dataRepository.Delete(skill);
            return Ok("Success");
        }
    }
}
