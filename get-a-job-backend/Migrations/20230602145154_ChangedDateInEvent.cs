﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace get_a_job_backend.Migrations
{
    public partial class ChangedDateInEvent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "event_date",
                table: "Events",
                newName: "event_stop_date");

            migrationBuilder.AddColumn<DateTime>(
                name: "event_start_date",
                table: "Events",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "event_start_date",
                table: "Events");

            migrationBuilder.RenameColumn(
                name: "event_stop_date",
                table: "Events",
                newName: "event_date");
        }
    }
}
