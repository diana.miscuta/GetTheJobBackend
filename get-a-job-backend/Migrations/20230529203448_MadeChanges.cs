﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace get_a_job_backend.Migrations
{
    public partial class MadeChanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    user_id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    user_email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    user_password = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    user_firstname = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    user_lastname = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    user_goal_1 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    user_goal_2 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    user_goal_3 = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.user_id);
                });

            migrationBuilder.CreateTable(
                name: "Checklists",
                columns: table => new
                {
                    checklist_id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    checklist_name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    user_id = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Checklists", x => x.checklist_id);
                    table.ForeignKey(
                        name: "FK_Checklists_Users_user_id",
                        column: x => x.user_id,
                        principalTable: "Users",
                        principalColumn: "user_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Events",
                columns: table => new
                {
                    event_id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    event_name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    event_date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    event_location = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    event_details = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    event_color = table.Column<int>(type: "int", nullable: false),
                    user_id = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Events", x => x.event_id);
                    table.ForeignKey(
                        name: "FK_Events_Users_user_id",
                        column: x => x.user_id,
                        principalTable: "Users",
                        principalColumn: "user_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Resumes",
                columns: table => new
                {
                    resume_id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    user_id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    resume_name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    creation_date = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Resumes", x => x.resume_id);
                    table.ForeignKey(
                        name: "FK_Resumes_Users_user_id",
                        column: x => x.user_id,
                        principalTable: "Users",
                        principalColumn: "user_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Sections",
                columns: table => new
                {
                    section_id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    section_name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    status = table.Column<bool>(type: "bit", nullable: false),
                    user_id = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sections", x => x.section_id);
                    table.ForeignKey(
                        name: "FK_Sections_Users_user_id",
                        column: x => x.user_id,
                        principalTable: "Users",
                        principalColumn: "user_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ChecklistItems",
                columns: table => new
                {
                    item_id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    checklist_id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    text = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    status = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChecklistItems", x => x.item_id);
                    table.ForeignKey(
                        name: "FK_ChecklistItems_Checklists_checklist_id",
                        column: x => x.checklist_id,
                        principalTable: "Checklists",
                        principalColumn: "checklist_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Educations",
                columns: table => new
                {
                    education_id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    resume_id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    study_field = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    institution = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    education_country = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    education_city = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    start_date = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    end_date = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    education_description = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Educations", x => x.education_id);
                    table.ForeignKey(
                        name: "FK_Educations_Resumes_resume_id",
                        column: x => x.resume_id,
                        principalTable: "Resumes",
                        principalColumn: "resume_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Languages",
                columns: table => new
                {
                    language_id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    resume_id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    language_name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    language_level = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Languages", x => x.language_id);
                    table.ForeignKey(
                        name: "FK_Languages_Resumes_resume_id",
                        column: x => x.resume_id,
                        principalTable: "Resumes",
                        principalColumn: "resume_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PersonalInfos",
                columns: table => new
                {
                    personalinfo_id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    resume_id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    first_name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    last_name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    phone_number = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    linkedin_link = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    address = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    description = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PersonalInfos", x => x.personalinfo_id);
                    table.ForeignKey(
                        name: "FK_PersonalInfos_Resumes_resume_id",
                        column: x => x.resume_id,
                        principalTable: "Resumes",
                        principalColumn: "resume_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Skills",
                columns: table => new
                {
                    skill_id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    resume_id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    skill_name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Skills", x => x.skill_id);
                    table.ForeignKey(
                        name: "FK_Skills_Resumes_resume_id",
                        column: x => x.resume_id,
                        principalTable: "Resumes",
                        principalColumn: "resume_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WorkExperiences",
                columns: table => new
                {
                    work_id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    resume_id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    job_title = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    institution = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    job_country = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    job_city = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    start_date = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    end_date = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    job_description = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkExperiences", x => x.work_id);
                    table.ForeignKey(
                        name: "FK_WorkExperiences_Resumes_resume_id",
                        column: x => x.resume_id,
                        principalTable: "Resumes",
                        principalColumn: "resume_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Posts",
                columns: table => new
                {
                    post_id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    user_id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    section_id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    posted_date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    post_text = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    status = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Posts", x => x.post_id);
                    table.ForeignKey(
                        name: "FK_Posts_Sections_section_id",
                        column: x => x.section_id,
                        principalTable: "Sections",
                        principalColumn: "section_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Posts_Users_user_id",
                        column: x => x.user_id,
                        principalTable: "Users",
                        principalColumn: "user_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ChecklistItems_checklist_id",
                table: "ChecklistItems",
                column: "checklist_id");

            migrationBuilder.CreateIndex(
                name: "IX_Checklists_user_id",
                table: "Checklists",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_Educations_resume_id",
                table: "Educations",
                column: "resume_id");

            migrationBuilder.CreateIndex(
                name: "IX_Events_user_id",
                table: "Events",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_Languages_resume_id",
                table: "Languages",
                column: "resume_id");

            migrationBuilder.CreateIndex(
                name: "IX_PersonalInfos_resume_id",
                table: "PersonalInfos",
                column: "resume_id");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_section_id",
                table: "Posts",
                column: "section_id");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_user_id",
                table: "Posts",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_Resumes_user_id",
                table: "Resumes",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_Sections_user_id",
                table: "Sections",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_Skills_resume_id",
                table: "Skills",
                column: "resume_id");

            migrationBuilder.CreateIndex(
                name: "IX_WorkExperiences_resume_id",
                table: "WorkExperiences",
                column: "resume_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ChecklistItems");

            migrationBuilder.DropTable(
                name: "Educations");

            migrationBuilder.DropTable(
                name: "Events");

            migrationBuilder.DropTable(
                name: "Languages");

            migrationBuilder.DropTable(
                name: "PersonalInfos");

            migrationBuilder.DropTable(
                name: "Posts");

            migrationBuilder.DropTable(
                name: "Skills");

            migrationBuilder.DropTable(
                name: "WorkExperiences");

            migrationBuilder.DropTable(
                name: "Checklists");

            migrationBuilder.DropTable(
                name: "Sections");

            migrationBuilder.DropTable(
                name: "Resumes");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
