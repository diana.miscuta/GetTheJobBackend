using get_a_job_backend.Helpers;
using get_a_job_backend.Models;
using get_a_job_backend.Models.DataManagers;
using get_a_job_backend.Models.Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace get_a_job_backend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<GetTheJobContext>(opt => opt.UseSqlServer(Configuration["ConnectionString:GetTheJobDB"]));
            services.AddScoped<IDataRepository<User>, UserDataManager>();
            services.AddScoped<UserDataManager>();
            services.AddScoped<ChecklistDataManager>();
            services.AddScoped<ChecklistItemDataManager>();
            services.AddScoped<EventDataManager>();
            services.AddScoped<SectionDataManager>();
            services.AddScoped<PostDataManager>();
            services.AddScoped<ResumeDataManager>();
            services.AddScoped<PersonalInfoDataManager>();
            services.AddScoped<WorkExperienceDataManager>();
            services.AddScoped<EducationDataManager>();
            services.AddScoped<SkillDataManager>();
            services.AddScoped<LanguageDataManager>();
            services.AddScoped<JwtService>();
            services.AddControllers();
            services.AddCors(options =>
            {
                options.AddPolicy("MyPolicy", builder =>
                {
                    builder.WithOrigins("http://localhost:3000")
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowCredentials();
                });
            });
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "get_a_job_backend", Version = "v1" });
            });
            services.AddControllersWithViews(options => { options.SuppressAsyncSuffixInActionNames = false; });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "get_a_job_backend v1"));
            }

            app.UseRouting();

            app.UseCors("MyPolicy");

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
