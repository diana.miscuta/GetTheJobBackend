﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace get_a_job_backend.Models
{
    public class Checklist
    {
        [Key]
        [Column("checklist_id")]
        public Guid ChecklistId { get; set; }
        [Column("checklist_name")]
        public string ChecklistName { get; set; }
        [Column("user_id")]
        public Guid UserId { get; set; }
        [ForeignKey("UserId")]
        public User User { get; set; }
    }
}
