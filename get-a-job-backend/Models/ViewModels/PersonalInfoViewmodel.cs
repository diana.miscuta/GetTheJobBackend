﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace get_a_job_backend.Models.ViewModels
{
    public class PersonalInfoViewmodel
    {
        public Guid PersonalInfoId { get; set; }
        public Guid ResumeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string LinkedInLink { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
    }
}
