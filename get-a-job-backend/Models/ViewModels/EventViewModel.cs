﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace get_a_job_backend.Models.ViewModels
{
    public class EventViewModel
    {
        public Guid EventId { get; set; }
        public Guid UserId { get; set; }
        public string EventName { get; set; }
        public DateTime EventStartDate { get; set; }
        public DateTime EventStopDate { get; set; }
        public string EventLocation { get; set; }
        public string EventDetails { get; set; }
        public int EventColor { get; set; }
    }
}
