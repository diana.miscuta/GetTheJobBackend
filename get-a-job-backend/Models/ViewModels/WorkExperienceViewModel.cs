﻿using System.ComponentModel.DataAnnotations.Schema;
using System;

namespace get_a_job_backend.Models.ViewModels
{
    public class WorkExperienceViewModel
    {
        public Guid WorkId { get; set; }
        public Guid ResumeId { get; set; }
        public string JobTitle { get; set; }
        public string Institution { get; set; }
        public string JobCountry { get; set; }
        public string JobCity { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string JobDescription { get; set; }
    }
}
