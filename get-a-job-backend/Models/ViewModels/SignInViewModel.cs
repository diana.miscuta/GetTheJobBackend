﻿namespace get_a_job_backend.Models.ViewModels
{
    public class SignInViewModel
    {
        public string UserEmail { get; set; }
        public string UserPassword { get; set; }
    }
}
