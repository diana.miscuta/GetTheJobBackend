﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace get_a_job_backend.Models.ViewModels
{
    public class PostViewModel
    {
        public Guid PostId { get; set; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public Guid SectionId { get; set; }
        public DateTime PostedDate { get; set; }
        public string PostText { get; set; }
    }
}
