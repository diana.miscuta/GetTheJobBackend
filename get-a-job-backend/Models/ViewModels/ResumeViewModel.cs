﻿using System.ComponentModel.DataAnnotations.Schema;
using System;

namespace get_a_job_backend.Models.ViewModels
{
    public class ResumeViewModel
    {
        public Guid ResumeId { get; set; }
        public Guid UserId { get; set; }
        public string ResumeName { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
