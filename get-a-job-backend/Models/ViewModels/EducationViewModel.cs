﻿using System.ComponentModel.DataAnnotations.Schema;
using System;

namespace get_a_job_backend.Models.ViewModels
{
    public class EducationViewModel
    {
        public Guid EducationId { get; set; }
        public Guid ResumeId { get; set; }
        public string StudyField { get; set; }
        public string Institution { get; set; }
        public string EducationCountry { get; set; }
        public string EducationCity { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string EducationDescription { get; set; }
    }
}
