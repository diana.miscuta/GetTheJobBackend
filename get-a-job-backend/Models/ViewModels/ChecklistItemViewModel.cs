﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace get_a_job_backend.Models.ViewModels
{
    public class ChecklistItemViewModel
    {
        public Guid ItemId { get; set; }
        public Guid Checklistid { get; set; }
        public string Text { get; set; }
        public bool Status { get; set; }
    }
}
