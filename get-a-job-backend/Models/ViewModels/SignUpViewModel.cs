﻿namespace get_a_job_backend.Models.ViewModels
{
    public class SignUpViewModel
    {
        public string UserEmail { get; set; }
        public string UserPassword { get; set; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
    }
}
