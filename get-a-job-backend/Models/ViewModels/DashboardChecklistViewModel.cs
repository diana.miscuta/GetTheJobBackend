﻿using System;

namespace get_a_job_backend.Models.ViewModels
{
    public class DashboardChecklistViewModel
    {
        public Guid ChecklistId { get; set; }
        public string ChecklistName { get; set; }
        public float ChecklistStatus { get; set; }
    }
}
