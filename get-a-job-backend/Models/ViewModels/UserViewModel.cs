﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace get_a_job_backend.Models.ViewModels
{
    public class UserViewModel
    {
        public Guid UserId { get; set; }
        public string UserEmail { get; set; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public string UserGoal1 { get; set; }
        public string UserGoal2 { get; set; }
        public string UserGoal3 { get; set; }
    }
}
