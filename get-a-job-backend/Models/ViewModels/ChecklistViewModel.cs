﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace get_a_job_backend.Models.ViewModels
{
    public class ChecklistViewModel
    {
        public Guid ChecklistId { get; set; }
        public string ChecklistName { get; set; }
        public Guid UserId { get; set; }
    }
}
