﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace get_a_job_backend.Models.ViewModels
{
    public class SectionViewModel
    {
        public Guid SectionId { get; set; }
        public Guid UserId { get; set; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public string SectionName { get; set; }
        public bool Status { get; set; }
    }
}
