﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace get_a_job_backend.Models.ViewModels
{
    public class SkillViewModel
    {
        public Guid SkillId { get; set; }
        public Guid ResumeId { get; set; }
        public string SkillName { get; set; }
    }
}
