﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace get_a_job_backend.Models.ViewModels
{
    public class LanguageViewModel
    {
        public Guid LanguageId { get; set; }
        public Guid ResumeId { get; set; }
        public string LanguageName { get; set; }
        public string LanguageLevel { get; set; }
    }
}
