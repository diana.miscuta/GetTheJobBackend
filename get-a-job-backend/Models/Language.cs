﻿using Microsoft.Net.Http.Headers;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Cryptography.X509Certificates;

namespace get_a_job_backend.Models
{
    public class Language
    {
        [Key]
        [Column("language_id")]
        public Guid LanguageId { get; set; }
        [Column("resume_id")]
        public Guid ResumeId { get; set; }
        [ForeignKey("ResumeId")]
        public Resume Resume { get; set; }
        [Column("language_name")]
        public string LanguageName { get; set; }
        [Column("language_level")]
        public string LanguageLevel { get; set; }
    }
}
