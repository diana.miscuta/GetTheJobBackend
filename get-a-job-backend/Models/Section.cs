﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace get_a_job_backend.Models
{
    public class Section
    {
        [Key]
        [Column("section_id")]
        public Guid SectionId { get; set; }
        [Column("section_name")]
        public string SectionName { get; set; }
        [Column("status")]
        public bool Status { get; set; }
        [Column("user_id")]
        public Guid UserId { get; set; }
        [ForeignKey("UserId")]
        public User User { get; set; }

        
    }
}
