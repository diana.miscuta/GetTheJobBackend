﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Microsoft.VisualBasic;
using System;

namespace get_a_job_backend.Models
{
    public class Resume
    {
        [Key]
        [Column("resume_id")]
        public Guid ResumeId { get; set; }
        [Column("user_id")]
        public Guid UserId { get; set; }
        [ForeignKey("UserId")]
        public User User { get; set; }
        [Column("resume_name")]
        public string ResumeName { get; set; }
        [Column("creation_date")]
        public DateTime CreationDate { get; set; }
        
    }
}
