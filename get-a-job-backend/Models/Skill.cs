﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;

namespace get_a_job_backend.Models
{
    public class Skill
    {
        [Key]
        [Column("skill_id")]
        public Guid SkillId { get; set; }
        [Column("resume_id")]
        public Guid ResumeId { get; set; }
        [ForeignKey("ResumeId")]
        public Resume Resume { get; set; }
        [Column("skill_name")]
        public string SkillName { get; set; }
    }
}
