﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;

namespace get_a_job_backend.Models
{
    public class Education
    {
        [Key]
        [Column("education_id")]
        public Guid EducationId { get; set; }
        [Column("resume_id")]
        public Guid ResumeId { get; set; }
        [ForeignKey("ResumeId")]
        public Resume Resume { get; set; }
        [Column("study_field")]
        public string StudyField { get; set; }
        [Column("institution")]
        public string Institution { get; set; }
        [Column("education_country")]
        public string EducationCountry { get; set; }
        [Column("education_city")]
        public string EducationCity { get; set; }
        [Column("start_date")]
        public string StartDate { get; set; }
        [Column("end_date")]
        public string EndDate { get; set; }
        [Column("education_description")]
        public string EducationDescription { get; set; }
    }
}
