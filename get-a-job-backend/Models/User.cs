﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace get_a_job_backend.Models
{
    public class User
    {
        [Key]
        [Column("user_id")]
        public Guid UserId { get; set; }
        [Column("user_email")]
        public string UserEmail { get; set; }
        [Column("user_password")]
        [JsonIgnore]
        public string UserPassword { get; set; }
        [Column("user_firstname")]
        public string UserFirstName { get; set; }
        [Column("user_lastname")]
        public string UserLastName { get; set; }
        [Column("user_goal_1")]
        public string UserGoal1 { get; set; }
        [Column("user_goal_2")]
        public string UserGoal2 { get; set; }
        [Column("user_goal_3")]
        public string UserGoal3 { get; set; }

        public IList<Post> Posts { get; }
    }
}
