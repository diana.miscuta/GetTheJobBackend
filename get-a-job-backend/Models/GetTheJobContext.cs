﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace get_a_job_backend.Models
{
    public class GetTheJobContext : DbContext
    {
        public GetTheJobContext(DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Checklist> Checklists { get; set; }
        public DbSet<ChecklistItem> ChecklistItems { get; set; }
        public DbSet<Section> Sections { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Resume> Resumes { get; set; }
        public DbSet<PersonalInfo> PersonalInfos { get; set; }
        public DbSet<WorkExperience> WorkExperiences { get; set; }
        public DbSet<Education> Educations { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<Skill> Skills { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
            .HasMany(a => a.Posts)
            .WithOne(b => b.User)
            .HasForeignKey(b => b.UserId)
            .OnDelete(DeleteBehavior.ClientSetNull);

            modelBuilder.Entity<Post>()
                .HasOne(b => b.User)
                .WithMany(a => a.Posts)
                .HasForeignKey(b => b.UserId)
                .OnDelete(DeleteBehavior.ClientSetNull);
        }
    }
}
