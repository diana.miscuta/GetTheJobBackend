﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace get_a_job_backend.Models
{
    public class Post
    {
        [Key]
        [Column("post_id")]
        public Guid PostId { get; set; }
        [Column("user_id")]
        public Guid UserId { get; set; }
        public User User { get; set; }
        [Column("section_id")]
        public Guid SectionId { get; set; }
        [ForeignKey("SectionId")]
        public Section Section { get; set; }
        [Column("posted_date")]
        public DateTime PostedDate { get; set; }
        [Column("post_text")]
        public string PostText { get; set; }
        [Column("status")]
        public bool Status { get; set; }

    }
}
