﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;

namespace get_a_job_backend.Models
{
    public class WorkExperience
    {
        [Key]
        [Column("work_id")]
        public Guid WorkId { get; set; }
        [Column("resume_id")]
        public Guid ResumeId { get; set; }
        [ForeignKey("ResumeId")]
        public Resume Resume { get; set; }
        [Column("job_title")]
        public string JobTitle { get; set; }
        [Column("institution")]
        public string Institution { get; set; }
        [Column("job_country")]
        public string JobCountry { get; set; }
        [Column("job_city")]
        public string JobCity { get; set; }
        [Column("start_date")]
        public string StartDate { get; set; }
        [Column("end_date")]
        public string EndDate { get; set; }
        [Column("job_description")]
        public string JobDescription { get; set; }
    }
}
