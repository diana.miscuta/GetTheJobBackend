﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Microsoft.VisualBasic;
using System;

namespace get_a_job_backend.Models
{
    public class PersonalInfo
    {
        [Key]
        [Column("personalinfo_id")]
        public Guid PersonalInfoId { get; set; }
        [Column("resume_id")]
        public Guid ResumeId { get; set; }
        [ForeignKey("ResumeId")]
        public Resume Resume { get; set; }
        [Column("first_name")]
        public string FirstName { get; set; }
        [Column("last_name")]
        public string LastName { get; set; }
        [Column("email")]
        public string Email { get; set; }
        [Column("phone_number")]
        public string PhoneNumber { get; set; }
        [Column("linkedin_link")]
        public string LinkedInLink { get; set; }
        [Column("address")]
        public string Address { get; set; }
        [Column("description")]
        public string Description { get; set; }

    }
}
