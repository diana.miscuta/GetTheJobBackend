﻿using get_a_job_backend.Models.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace get_a_job_backend.Models.DataManagers
{
    public class ResumeDataManager : IDataRepository<Resume>
    {
        readonly GetTheJobContext _resumeContext;
        public ResumeDataManager(GetTheJobContext context)
        {
            _resumeContext = context;
        }
        public void Add(Resume resume)
        {
            _resumeContext.Resumes.Add(resume);
            _resumeContext.SaveChanges();
        }

        public void Delete(Resume resume)
        {
            _resumeContext.Resumes.Remove(resume);
            _resumeContext.SaveChanges();
        }

        public Resume Get(Guid id)
        {
            return _resumeContext.Resumes.Where(e => e.ResumeId == id).Include(u => u.User).FirstOrDefault();
        }

        public IEnumerable<Resume> GetAll()
        {
            return _resumeContext.Resumes.Include(u => u.User).ToList();
        }

        public IEnumerable<Resume> GetResumesByUserId(Guid id)
        {
            return _resumeContext.Resumes.Where(e => e.UserId == id).Include(u => u.User).ToList();
        }

        public void Update(Resume resume, Resume entity)
        {
            resume.ResumeName = entity.ResumeName;
            _resumeContext.SaveChanges();
        }

        public Resume Create(Resume resume)
        {
            _resumeContext.Resumes.Add(resume);
            resume.ResumeId = Guid.NewGuid(); 
            _resumeContext.SaveChanges();
            return resume;
        }
    }
}
