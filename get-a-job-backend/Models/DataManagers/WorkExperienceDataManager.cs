﻿using get_a_job_backend.Models.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace get_a_job_backend.Models.DataManagers
{
    public class WorkExperienceDataManager : IDataRepository<WorkExperience>
    {
        readonly GetTheJobContext _workExperienceContext;
        public WorkExperienceDataManager(GetTheJobContext context)
        {
            _workExperienceContext = context;
        }

        public void Add(WorkExperience workExperience)
        {
            _workExperienceContext.WorkExperiences.Add(workExperience);
            _workExperienceContext.SaveChanges();
        }

        public void Delete(WorkExperience workExperience)
        {
            _workExperienceContext.WorkExperiences.Remove(workExperience);
            _workExperienceContext.SaveChanges();
        }

        public WorkExperience Get(Guid id)
        {
            return _workExperienceContext.WorkExperiences.Where(e => e.WorkId == id).Include(r => r.Resume).FirstOrDefault();
        }

        public IEnumerable<WorkExperience> GetAll()
        {
            return _workExperienceContext.WorkExperiences.Include(r => r.Resume).ToList();
        }

        public WorkExperience GetWorkExperienceByResumeId(Guid id)
        {
            return _workExperienceContext.WorkExperiences.Where(e => e.ResumeId == id).Include(r => r.Resume).FirstOrDefault();
        }

        public void Update(WorkExperience workExperience, WorkExperience entity)
        {
            workExperience.JobTitle = entity.JobTitle;
            workExperience.Institution = entity.Institution;
            workExperience.JobCity = entity.JobCity;
            workExperience.JobCountry = entity.JobCountry;
            workExperience.JobDescription = entity.JobDescription;
            workExperience.StartDate = entity.StartDate;
            workExperience.EndDate = entity.EndDate;

            _workExperienceContext.SaveChanges();
        }

        public WorkExperience Create(WorkExperience workExperience)
        {
            _workExperienceContext.WorkExperiences.Add(workExperience);
            workExperience.WorkId = Guid.NewGuid(); 
            _workExperienceContext.SaveChanges();
            return workExperience;
        }
    }
}
