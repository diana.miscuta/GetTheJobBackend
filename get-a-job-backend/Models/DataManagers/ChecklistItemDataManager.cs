﻿using get_a_job_backend.Models.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace get_a_job_backend.Models.DataManagers
{
    public class ChecklistItemDataManager : IDataRepository<ChecklistItem>
    {
        readonly GetTheJobContext _checklistItemContext;
        public ChecklistItemDataManager(GetTheJobContext context)
        {
            _checklistItemContext = context;
        }
        public void Add(ChecklistItem item)
        {
            _checklistItemContext.ChecklistItems.Add(item);
            _checklistItemContext.SaveChanges();
        }

        public void Delete(ChecklistItem item)
        {
            _checklistItemContext.ChecklistItems.Remove(item);
            _checklistItemContext.SaveChanges();
        }

        public ChecklistItem Get(Guid id)
        {
            return _checklistItemContext.ChecklistItems.Where(e => e.ItemId == id).Include(c => c.Checklist).FirstOrDefault();
        }

        public IEnumerable<ChecklistItem> GetAll()
        {
            return _checklistItemContext.ChecklistItems.Include(c => c.Checklist).ToList();
        }

        public IEnumerable<ChecklistItem> GetByChecklistId(Guid id)
        {
            return _checklistItemContext.ChecklistItems.Where(e => e.ChecklistId == id).Include(c => c.Checklist).ToList();
        }

        public void Update(ChecklistItem item, ChecklistItem entity)
        {
            item.Text = entity.Text;
            item.Status = entity.Status;

            _checklistItemContext.SaveChanges();
        }

        public ChecklistItem Create(ChecklistItem item)
        {
            _checklistItemContext.ChecklistItems.Add(item);
            item.ItemId = Guid.NewGuid(); 
            _checklistItemContext.SaveChanges();
            return item;
        }
    }
}
