﻿using get_a_job_backend.Models.Repository;
using get_a_job_backend.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;

namespace get_a_job_backend.Models.DataManagers
{
    public class PostDataManager : IDataRepository<Post>
    {
        readonly GetTheJobContext _postContext;
        public PostDataManager(GetTheJobContext context)
        {
            _postContext = context;
        }

        public void Add(Post post)
        {
            _postContext.Posts.Add(post);
            _postContext.SaveChanges();
        }

        public void Delete(Post post)
        {
            _postContext.Posts.Remove(post);
            _postContext.SaveChanges();
        }

        public Post Get(Guid id)
        {
            return _postContext.Posts.Where(x => x.PostId == id).Include(u => u.User).Include(s => s.Section).FirstOrDefault();
        }

        public IEnumerable<Post> GetAll()
        {
            return _postContext.Posts.Include(u => u.User).Include(s => s.Section).ToList();
        }

        public IEnumerable<PostViewModel> GetPostsBySectionIdPublished(Guid id)
        {
            var posts = _postContext.Posts
                .Join(_postContext.Users, p => p.UserId, u => u.UserId, (p, u) => new
                {
                    userId = p.UserId,
                    userFirstName = u.UserFirstName,
                    userLastName = u.UserLastName,
                    postId = p.PostId,
                    sectionId = p.SectionId,
                    postText = p.PostText,
                    createdAt = p.PostedDate,
                    status = p.Status
                })
                .Where(x => x.status == true)
                .Where(x => x.sectionId == id)
                .Select(x => new PostViewModel()
                {
                    PostId = x.postId,
                    SectionId = x.sectionId,
                    UserFirstName = x.userFirstName,
                    UserLastName = x.userLastName,
                    PostText = x.postText,
                    PostedDate = x.createdAt
                })
                .OrderByDescending(x => x.PostedDate)
                .ToList();

            return posts;
        }

        public IEnumerable<PostViewModel> GetPostsBySectionIdPending(Guid id)
        {
            var posts = _postContext.Posts
                .Join(_postContext.Users, p => p.UserId, u => u.UserId, (p, u) => new
                {
                    userId = p.UserId,
                    userFirstName = u.UserFirstName,
                    userLastName = u.UserLastName,
                    postId = p.PostId,
                    sectionId = p.SectionId,
                    postText = p.PostText,
                    createdAt = p.PostedDate,
                    status = p.Status
                })
                .Where(x => x.status == false)
                .Where(x => x.sectionId == id)
                .Select(x => new PostViewModel()
                {
                    PostId = x.postId,
                    SectionId = x.sectionId,
                    UserFirstName = x.userFirstName,
                    UserLastName = x.userLastName,
                    PostText = x.postText,
                    PostedDate = x.createdAt
                })
                .OrderByDescending(x => x.PostedDate)
                .ToList();

            return posts;
        }

        public void Update(Post post, Post entity)
        {
            post.Status = entity.Status;

            _postContext.SaveChanges();
        }

        public Post Create(Post post)
        {
            _postContext.Posts.Add(post);
            post.PostId = Guid.NewGuid();
            _postContext.SaveChanges();
            return post;
        }
    }
}
