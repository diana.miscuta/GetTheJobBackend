﻿using get_a_job_backend.Models.Repository;
using get_a_job_backend.Models.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace get_a_job_backend.Models.DataManagers
{
    public class ChecklistDataManager : IDataRepository<Checklist>
    {
        readonly GetTheJobContext _checklistContext;
        public ChecklistDataManager(GetTheJobContext context)
        {
            _checklistContext = context;
        }
        public void Add(Checklist checklist)
        {
            _checklistContext.Checklists.Add(checklist);
            _checklistContext.SaveChanges();
        }

        public void Delete(Checklist checklist)
        {
            _checklistContext.Checklists.Remove(checklist);
            _checklistContext.SaveChanges();
        }

        public Checklist Get(Guid id)
        {
            return _checklistContext.Checklists.Where(e => e.ChecklistId == id).Include(u => u.User).FirstOrDefault();
        }

        public IEnumerable<Checklist> GetAll()
        {
            return _checklistContext.Checklists.Include(u => u.User).ToList();
        }

        public IEnumerable<Checklist> GetChecklistsByUserId(Guid id)
        {
            return _checklistContext.Checklists.Where(e => e.UserId == id).Include(u => u.User).ToList();
        }

        public IEnumerable<DashboardChecklistViewModel> GetChecklistsForDashboard(Guid id)
        {
            var checklists = _checklistContext.Checklists.Where(e => e.UserId == id).Include(u => u.User).ToList();

            var list = new List<DashboardChecklistViewModel>();

            foreach( var checklist in checklists)
            {
                var totalTasks = _checklistContext.ChecklistItems.Where(e => e.ChecklistId == checklist.ChecklistId).Count();
                var completedTasks = _checklistContext.ChecklistItems.Where(e => e.ChecklistId == checklist.ChecklistId).Count(s => s.Status == true);

                float checklistStatus = totalTasks > 0 ? ((completedTasks * 100) / totalTasks) : 0;

                var newDashboardChecklist = new DashboardChecklistViewModel
                {
                    ChecklistId = checklist.ChecklistId,
                    ChecklistName = checklist.ChecklistName,
                    ChecklistStatus = checklistStatus,
                };

                list.Add(newDashboardChecklist);
            }

            return list;
        }

        public void Update(Checklist checklist, Checklist entity)
        {
            checklist.ChecklistName = entity.ChecklistName;

            _checklistContext.SaveChanges();
        }

        public Checklist Create(Checklist checklist)
        {
            _checklistContext.Checklists.Add(checklist);
            checklist.ChecklistId = Guid.NewGuid(); 
            _checklistContext.SaveChanges();
            return checklist;
        }


    }
}
