﻿using get_a_job_backend.Models.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace get_a_job_backend.Models.DataManagers
{
    public class UserDataManager : IDataRepository<User>
    {
        readonly GetTheJobContext _userContext;
        public UserDataManager (GetTheJobContext context)
        {
            _userContext = context;
        }

        public void Add(User user)
        {
            _userContext.Users.Add(user);
            _userContext.SaveChanges();
        }

        public void Delete(User user)
        {
            _userContext.Users.Remove(user);
            _userContext.SaveChanges();
        }

        public User Get(Guid id)
        {
            return _userContext.Users.Where(e => e.UserId == id).FirstOrDefault();
        }

        public User GetByEmail(string email)
        {
            return _userContext.Users.FirstOrDefault(u => u.UserEmail == email);
        }

        public IEnumerable<User> GetAll()
        {
            return _userContext.Users.ToList();
        }

        public void Update(User user, User entity)
        {
            user.UserGoal1 = entity.UserGoal1;
            user.UserGoal2 = entity.UserGoal2;
            user.UserGoal3 = entity.UserGoal3;

            _userContext.SaveChanges();
        }

        public User Create(User user)
        {
            _userContext.Users.Add(user);
            user.UserId = Guid.NewGuid();
            _userContext.SaveChanges();
            return user;
        }

        public User ExistingAccount(string email)
        {
            var user = _userContext.Users.FirstOrDefault(u => u.UserEmail.Equals(email));

            return user;
        }

        public bool IsEmailValid(string email)
        {
            string regex = @"^[^@\s]+@[^@\s]+\.(com|net|org|gov)$";

            return Regex.IsMatch(email, regex, RegexOptions.IgnoreCase);
        }
    }
}
