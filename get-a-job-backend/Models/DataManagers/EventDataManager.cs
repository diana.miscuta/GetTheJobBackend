﻿using get_a_job_backend.Models.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace get_a_job_backend.Models.DataManagers
{
    public class EventDataManager : IDataRepository<Event>
    {
        readonly GetTheJobContext _eventContext;
        public EventDataManager(GetTheJobContext context)
        {
            _eventContext = context;
        }

        public void Add(Event entity)
        {
            _eventContext.Events.Add(entity);
            _eventContext.SaveChanges();
        }

        public void Delete(Event entity)
        {
            _eventContext.Events.Remove(entity);
            _eventContext.SaveChanges();
        }

        public Event Get(Guid id)
        {
            return _eventContext.Events.Where(x => x.EventId == id).Include(u => u.User).FirstOrDefault();
        }

        public IEnumerable<Event> GetAll()
        {
            return _eventContext.Events.Include(u => u.User).ToList();
        }

        public IEnumerable<Event> GetEventsByUserId(Guid id)
        {
            return _eventContext.Events.Where(x => x.UserId == id).Include(u => u.User).ToList();
        }

        public void Update(Event dbEvent, Event entity)
        {
            dbEvent.EventName = entity.EventName;
            dbEvent.EventStartDate = entity.EventStartDate;
            dbEvent.EventStopDate = entity.EventStopDate;
            dbEvent.EventLocation = entity.EventLocation;
            dbEvent.EventColor = entity.EventColor;
            dbEvent.EventDetails = entity.EventDetails;

            _eventContext.SaveChanges();
        }

        public Event Create(Event entity)
        {
            _eventContext.Events.Add(entity);
            entity.EventId = Guid.NewGuid();
            _eventContext.SaveChanges();
            return entity;
        }
    }
}
