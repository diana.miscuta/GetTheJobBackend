﻿using get_a_job_backend.Models.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace get_a_job_backend.Models.DataManagers
{
    public class EducationDataManager : IDataRepository<Education>
    {
        readonly GetTheJobContext _educationContext;
        public EducationDataManager(GetTheJobContext context)
        {
            _educationContext = context;
        }

        public void Add(Education education)
        {
            _educationContext.Educations.Add(education);
            _educationContext.SaveChanges();
        }

        public void Delete(Education education)
        {
            _educationContext.Educations.Remove(education);
            _educationContext.SaveChanges();
        }

        public Education Get(Guid id)
        {
            return _educationContext.Educations.Where(e => e.EducationId == id).Include(r => r.Resume).FirstOrDefault();
        }

        public IEnumerable<Education> GetAll()
        {
            return _educationContext.Educations.Include(r => r.Resume).ToList();
        }

        public Education GetEducationByResumeId(Guid id)
        {
            return _educationContext.Educations.Where(e => e.ResumeId == id).Include(r => r.Resume).FirstOrDefault();
        }

        public void Update(Education education, Education entity)
        {
            education.Institution = entity.Institution;
            education.StudyField = entity.StudyField;
            education.EducationCity = entity.EducationCity;
            education.EducationCountry = entity.EducationCountry;
            education.EducationDescription = entity.EducationDescription;
            education.StartDate = entity.StartDate;
            education.EndDate = entity.EndDate;

            _educationContext.SaveChanges();
        }

        public Education Create(Education education)
        {
            _educationContext.Educations.Add(education);
            education.EducationId = Guid.NewGuid();
            _educationContext.SaveChanges();
            return education;
        }
    }
}
