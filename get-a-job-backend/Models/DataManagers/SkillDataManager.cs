﻿using get_a_job_backend.Models.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace get_a_job_backend.Models.DataManagers
{
    public class SkillDataManager : IDataRepository<Skill>
    {
        readonly GetTheJobContext _skillContext;
        public SkillDataManager(GetTheJobContext context)
        {
            _skillContext = context;
        }
        public void Add(Skill skill)
        {
            _skillContext.Skills.Add(skill);
            _skillContext.SaveChanges();
        }

        public void AddSkills(List<Skill> skills)
        {
            foreach (Skill skill in skills)
            {
                _skillContext.Skills.Add(skill);
            }
            _skillContext.SaveChanges();
        }

        public void Delete(Skill skill)
        {
            _skillContext.Skills.Remove(skill);
            _skillContext.SaveChanges();
        }

        public Skill Get(Guid id)
        {
            return _skillContext.Skills.Where(e => e.SkillId == id).Include(r => r.Resume).FirstOrDefault();
        }

        public IEnumerable<Skill> GetAll()
        {
            return _skillContext.Skills.Include(r => r.Resume).ToList();
        }
        public IEnumerable<Skill> GetSkillsByResumeId(Guid id)
        {
            return _skillContext.Skills.Where(e => e.ResumeId == id).Include(r => r.Resume).ToList();
        }

        public void Update(Skill skill, Skill entity)
        {
            skill.SkillName = entity.SkillName;
            _skillContext.SaveChanges();
        }

        public void Create(Skill skill)
        {
            _skillContext.Skills.Add(skill);
            _skillContext.SaveChanges();
        }
    }
}
