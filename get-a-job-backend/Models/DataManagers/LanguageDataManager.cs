﻿using get_a_job_backend.Models.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace get_a_job_backend.Models.DataManagers
{
    public class LanguageDataManager : IDataRepository<Language>
    {
        readonly GetTheJobContext _languageContext;
        public LanguageDataManager(GetTheJobContext context)
        {
            _languageContext = context;
        }
        public void Add(Language language)
        {
            _languageContext.Languages.Add(language);
            _languageContext.SaveChanges();
        }

        public void AddLanguages(List<Language> languages)
        {
            foreach (Language language in languages)
            {
                _languageContext.Languages.Add(language);
            }
            _languageContext.SaveChanges();
        }

        public void Delete(Language language)
        {
            _languageContext?.Languages.Remove(language);
            _languageContext.SaveChanges();
        }

        public Language Get(Guid id)
        {
            return _languageContext.Languages.Where(e => e.LanguageId == id).Include(r => r.Resume).FirstOrDefault();
        }

        public IEnumerable<Language> GetAll()
        {
            return _languageContext.Languages.Include(r => r.Resume).ToList();
        }
        public IEnumerable<Language> GetLanguagesByResumeId(Guid id)
        {
            return _languageContext.Languages.Where(e => e.ResumeId == id).Include(r => r.Resume).ToList();
        }

        public void Update(Language language, Language entity)
        {
            language.LanguageName = entity.LanguageName;
            language.LanguageLevel = entity.LanguageLevel;
            _languageContext.SaveChanges();
        }

        public void Create(Language language)
        {
            _languageContext.Languages.Add(language);
            _languageContext.SaveChanges();
        }
    }
}
