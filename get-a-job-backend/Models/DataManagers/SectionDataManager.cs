﻿using get_a_job_backend.Models.Repository;
using get_a_job_backend.Models.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace get_a_job_backend.Models.DataManagers
{
    public class SectionDataManager : IDataRepository<Section>
    {
        readonly GetTheJobContext _sectionContext;
        public SectionDataManager(GetTheJobContext context)
        {
            _sectionContext = context;
        }

        public void Add(Section section)
        {
            _sectionContext.Sections.Add(section);
            _sectionContext.SaveChanges();
        }

        public void Delete(Section section)
        {
            _sectionContext.Sections.Remove(section);
            _sectionContext.SaveChanges();
        }

        public Section Get(Guid id)
        {
            return _sectionContext.Sections.Where(x => x.SectionId == id).Include(u => u.User).FirstOrDefault();
        }

        public IEnumerable<Section> GetAll()
        {
            return _sectionContext.Sections.Include(u => u.User).ToList();
        }

        public IEnumerable<Section> GetAllPublished()
        {
            var sections = _sectionContext.Sections
                .Where(x => x.Status == true)
                .Include(u => u.User)
                .ToList();

            return sections;
        }

        public IEnumerable<Section> GetAllPending()
        {
            var sections = _sectionContext.Sections.Where(x => x.Status == false).Include(u => u.User).ToList();

            return sections;
        }

        public void Update(Section section, Section entity)
        {
            section.Status = entity.Status;

            _sectionContext.SaveChanges();
        }

        public Section Create(Section section)
        {
            _sectionContext.Sections.Add(section);
            
            section.SectionId = Guid.NewGuid();
            _sectionContext.SaveChanges();
            return section;
        }
    }
}
