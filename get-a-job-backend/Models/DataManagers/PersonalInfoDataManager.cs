﻿using get_a_job_backend.Models.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace get_a_job_backend.Models.DataManagers
{
    public class PersonalInfoDataManager : IDataRepository<PersonalInfo>
    {
        readonly GetTheJobContext _personalInfoContext;
        public PersonalInfoDataManager(GetTheJobContext context)
        {
            _personalInfoContext = context;
        }

        public void Add(PersonalInfo personalInfo)
        {
            _personalInfoContext.PersonalInfos.Add(personalInfo);
            _personalInfoContext.SaveChanges();
        }

        public void Delete(PersonalInfo personalInfo)
        {
            _personalInfoContext.PersonalInfos.Remove(personalInfo);
            _personalInfoContext.SaveChanges();
        }

        public PersonalInfo Get(Guid id)
        {
            return _personalInfoContext.PersonalInfos.Where(e => e.PersonalInfoId == id).Include(r => r.Resume).FirstOrDefault();
        }

        public IEnumerable<PersonalInfo> GetAll()
        {
            return _personalInfoContext.PersonalInfos.Include(r => r.Resume).ToList();
        }

        public PersonalInfo GetPersonalInfoByResumeId(Guid id)
        {
            return _personalInfoContext.PersonalInfos.Where(e => e.ResumeId == id).Include(r => r.Resume).FirstOrDefault();
        }

        public void Update(PersonalInfo personalInfo, PersonalInfo entity)
        {
            personalInfo.FirstName = entity.FirstName;
            personalInfo.LastName = entity.LastName;
            personalInfo.PhoneNumber = entity.PhoneNumber;
            personalInfo.Email = entity.Email;
            personalInfo.LinkedInLink = entity.LinkedInLink;
            personalInfo.Address = entity.Address;
            personalInfo.Description = entity.Description;

            _personalInfoContext.SaveChanges();
        }

        public PersonalInfo Create(PersonalInfo personalInfo)
        {
            _personalInfoContext.PersonalInfos.Add(personalInfo);
            personalInfo.PersonalInfoId = Guid.NewGuid();
            _personalInfoContext.SaveChanges();
            return personalInfo;
        }
    }
}
