﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace get_a_job_backend.Models
{
    public class Event
    {
        [Key]
        [Column("event_id")]
        public Guid EventId { get; set; }
        [Column("event_name")]
        public string EventName { get; set; }
        [Column("event_start_date")]
        public DateTime EventStartDate { get; set; }
        [Column("event_stop_date")]
        public DateTime EventStopDate { get; set; }
        [Column("event_location")]
        public string EventLocation { get; set; }
        [Column("event_details")]
        public string EventDetails { get; set; }
        [Column("event_color")]
        public int EventColor { get; set; }
        [Column("user_id")]
        public Guid UserId { get; set; }
        [ForeignKey("UserId")]
        public User User { get; set; }
    }
}
