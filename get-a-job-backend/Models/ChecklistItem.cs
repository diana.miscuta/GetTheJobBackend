﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace get_a_job_backend.Models
{
    public class ChecklistItem
    {
        [Key]
        [Column("item_id")]
        public Guid ItemId { get; set; }
        [Column("checklist_id")]
        public Guid ChecklistId { get; set; }
        [ForeignKey("ChecklistId")]
        public Checklist Checklist { get; set; }
        [Column("text")]
        public string Text { get; set; }
        [Column("status")]
        public bool Status { get; set; }
    }
}
